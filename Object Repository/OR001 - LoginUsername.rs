<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR001 - LoginUsername</name>
   <tag></tag>
   <elementGuidId>7549d8b2-0eab-495c-96ec-ff8160ca8a51</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;app&quot;]/div[1]/div/div[1]/div/div[2]/div[2]/form/div[1]/div/div[2]/input</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#app > div.orangehrm-login-layout > div > div.orangehrm-login-container > div > div.orangehrm-login-slot > div.orangehrm-login-form > form > div:nth-child(2) > div > div:nth-child(2) > input</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@name = 'username']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>username</value>
      <webElementGuid>4984782a-e6e3-454a-a859-47ae5f0c67c4</webElementGuid>
   </webElementProperties>
</WebElementEntity>
