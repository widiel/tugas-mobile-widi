<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OR004 - LogoOrange</name>
   <tag></tag>
   <elementGuidId>53792287-25d4-4dbf-96d2-be177f10e7d6</elementGuidId>
   <imagePath></imagePath>
   <selectorCollection>
      <entry>
         <key>IMAGE</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#app > div.oxd-layout > div.oxd-layout-navigation > aside > nav > div.oxd-sidepanel-header > a > div.oxd-brand-banner > img</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
