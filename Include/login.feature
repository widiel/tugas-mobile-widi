#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Login saucelabs
  user want to login in website saucedemo

  @LGN001
  Scenario: User want to login with valid credentials
    Given open website and redirect to saucedemo
    When user enter username "standard_user"
    And user enter password "secret_sauce"
    And click button login
    Then user verify success to dashboard home

  @LGN002
  Scenario: user not input username and password
    Given open website and redirect to saucedemo
    When User enter username ""
    And User enter password ""
    And click button login
    Then User verify failed message error "Epic sadface: Password is required"
