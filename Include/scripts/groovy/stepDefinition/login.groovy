package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When


import internal.GlobalVariable

public class login {
	@Given("open website and redirect to saucedemo")
	public void open_website_and_redirect_to_saucedemo() {
		WebUI.openBrowser('')
		WebUI.navigateToUrl(GlobalVariable.saucedemo)
	}

	@When("user enter username {username}")
	public void user_enter_username(String username) {

		WebUI.setText(findTestObject('Object Repository/Page_Swag Labs/fieldUsername'), username)
	}

	@When("user enter password {password}")
	public void user_enter_password(String password) {
		WebUI.setText(findTestObject('Object Repository/Page_Swag Labs/fieldPassword'), password)
	}

	@When("click button login")
	public void click_button_login() {
		WebUI.click(findTestObject)('Object Repository/Page_Swag Labs/loginButton')
		// Write code here that turns the phrase above into concrete actions

	}

	@Then("user verify success to dashboard home")
	public void user_verify_success_to_dashboard_home() {
		WebUI.verifyElementVisible(findTestObject('Object Repository/Page_Swag Labs/icon_swag')
				// Write code here that turns the phrase above into concrete actions

			}

	@When("User enter username {string}")
	public void user_enter_username(String string) {
		// Write code here that turns the phrase above into concrete actions

	}

	@When("User enter password {string}")
	public void user_enter_password(String string) {
		// Write code here that turns the phrase above into concrete actions

	}

	@Then("User verify failed message error {string}")
	public void user_verify_failed_message_error(String string) {
		// Write code here that turns the phrase above into concrete actions

	}
}
