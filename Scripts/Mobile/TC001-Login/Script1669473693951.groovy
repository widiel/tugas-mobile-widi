import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('C:\\Users\\widi pamungkas\\Katalon Studio\\latihan-katalon\\apk\\Android.SauceLabs.Mobile.Sample.app.2.7.1 (1).apk', 
    true)

Mobile.setText(findTestObject('mobile_automation/enter_username'), 'standard_user', 1)

Mobile.setText(findTestObject('mobile_automation/enter_password'), 'secret_sauce', 1)

Mobile.tap(findTestObject('mobile_automation/btn_login'), 5)

Mobile.takeScreenshot('C:\\Users\\widi pamungkas\\Katalon Studio\\latihan-katalon\\images', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('mobile_automation/mobile2/klikSuaceLabsBike'), 0)

Mobile.tap(findTestObject('mobile_automation/mobile2/btnAddToCart'), 5)

Mobile.tap(findTestObject('mobile_automation/mobile2/addKeranjang'), 8)

Mobile.tap(findTestObject('mobile_automation/mobile2/btnCheckOut'), 3)

Mobile.setText(findTestObject('mobile_automation/mobile2/fieldFirstName'), 'widi', 0)

Mobile.setText(findTestObject('mobile_automation/mobile2/fieldLastName'), 'binar', 0)

Mobile.setText(findTestObject('mobile_automation/mobile2/fieldZIP'), '46156', 0)

Mobile.tap(findTestObject('mobile_automation/mobile2/btnContinue'), 0)

Mobile.tap(findTestObject('mobile_automation/mobile2/btnFinish'), 0)

Mobile.tap(findTestObject('mobile_automation/mobile2/btnBackHome'), 0)

Mobile.tap(findTestObject('mobile_automation/mobile2/klikIconMenu'), 0)

Mobile.tap(findTestObject('mobile_automation/mobile2/btnLogout'), 0)

Mobile.closeApplication()

