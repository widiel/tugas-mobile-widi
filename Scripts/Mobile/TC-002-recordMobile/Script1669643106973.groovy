import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('C:\\Users\\widi pamungkas\\Katalon Studio\\latihan-katalon\\apk\\Android.SauceLabs.Mobile.Sample.app.2.7.1 (1).apk', 
    true)

Mobile.setText(findTestObject('recordMobile/android.widget.EditText - standarx'), 'standard_user', 0)

Mobile.setText(findTestObject('Object Repository/recordMobile/android.widget.EditText - Password'), 'secret_sauce', 0)

Mobile.tap(findTestObject('Object Repository/recordMobile/android.view.ViewGroup'), 0)

Mobile.tap(findTestObject('Object Repository/recordMobile/android.widget.ImageView'), 0)

Mobile.tap(findTestObject('Object Repository/recordMobile/android.view.ViewGroup (1)'), 0)

Mobile.tap(findTestObject('Object Repository/recordMobile/android.view.ViewGroup (2)'), 0)

Mobile.tap(findTestObject('Object Repository/recordMobile/android.widget.TextView - CHECKOUT'), 0)

Mobile.setText(findTestObject('Object Repository/recordMobile/android.widget.EditText - First Name'), 'widi el', 0)

Mobile.setText(findTestObject('Object Repository/recordMobile/android.widget.EditText - Last Name'), 'jakarta', 0)

Mobile.setText(findTestObject('Object Repository/recordMobile/android.widget.EditText - ZipPostal Code'), '46156', 0)

Mobile.tap(findTestObject('Object Repository/recordMobile/android.widget.TextView - CONTINUE'), 0)

Mobile.tap(findTestObject('Object Repository/recordMobile/android.widget.TextView - FINISH'), 0)

Mobile.tap(findTestObject('Object Repository/recordMobile/android.view.ViewGroup (3)'), 0)

Mobile.tap(findTestObject('Object Repository/recordMobile/android.widget.ImageView (1)'), 0)

Mobile.tap(findTestObject('Object Repository/recordMobile/android.widget.TextView - 29.99'), 0)

Mobile.takeScreenshot('C:\\Users\\WIDIPA~1\\AppData\\Local\\Temp\\screenshot83281168322989836.png')

Mobile.closeApplication()

